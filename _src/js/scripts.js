$(function() {
  var elInput = $('.form__input');
  var elMessage = $('.form__message');
  var elButton = $('.form__button');

  elButton.on('click' , function(e) {

    elInput.each(function() {
      var input = $(this);

      if ($.trim(input.val()) === '') {
        input.addClass('has-error')
        .prev(elMessage)
        .removeClass('is-invisible')
        .addClass('is-visible');
      } else {
        input.removeClass('has-error')
        .prev(elMessage)
        .removeClass('is-visible')
        .addClass('is-invisible');
      }
    });
    e.preventDefault();
  });

  elInput.on('blur', function() {
    var input = $(this);

    if (input.val() != '') {
      input.removeClass('has-error')
      .prev(elMessage)
      .removeClass('is-visible')
      .addClass('is-invisible');
    } else {
      input.addClass('has-error')
      .prev(elMessage)
      .removeClass('is-invisible')
      .addClass('is-visible');
    }
  });
});
